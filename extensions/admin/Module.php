<?php
namespace openadm\admin\extensions\admin;

use Yii;
/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'openadm\admin\extensions\admin\controllers';

    public $defaultRoute = 'dashboard/index';

    public $packageInstalledPath = '@app/extensions';

    public $packageScanPath = '@storage/extensions';

    public $rootProjectPath = '@root';

    public $composerPath = '/usr/bin/composer';

    public $extConfig = [];

    public $composerDetects = [
        '/usr/local/bin/composer',
        '/usr/bin/composer'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        foreach ($this->composerDetects as $bin){
            if(is_executable($bin)){
                $this->extConfig['composerPath'] = $bin;
            }
        }

        // custom initialization code goes here
    }
}
