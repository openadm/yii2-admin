openadm/yii2-admin
=========

- introduce 
---
基于yii2-extension包管理和adminlte2主题构建的admin项目，包括了基础的用户管理，RBAC管理，扩展管理等核心功能。

[去OpenADM（https://openadm.com）查看更多](https://openadm.com "去OpenADM查看更多")

- Source
---
Source [https://gitee.com/openadm/yii2-admin.git](https://gitee.com/openadm/yii2-admin.git "Gitee")

- Installation
---
The preferred way to install this extension is through composer.

Either run

```php
php composer.phar require --prefer-dist openadm/yii2-admin "*"
```


or add

```php
"openadm/yii2-admin": "*"
```


to the require section of your composer.json file.

- Usage
---
Once the extension is installed, simply use it in your code by :


- Migrate DB
---

```php
./yii migrate --migrationPath=@openadm/admin/migrations
./yii migrate all -p=@tecnocen/oauth2server/migrations/tables
```

- screenshots
---
[![文章模块](https://openadm.com/upload/default/down?path=%2F1%2FeUd4l9IbwawbFN9m4gAzK901bzdXkFdJ.png "文章模块")](https://openadm.com "文章模块")

[![文章模块](https://openadm.com/upload/default/down?path=%2F1%2F9EFYYbX1vDHF7d0NMaqFx5VCk-GKNng_.png "菜单模块")](https://openadm.com "菜单模块")
