<?php
namespace openadm\admin\controllers;

use Yii;
use yii2mod\rbac\filters\AccessControl;
use yii\base\UserException;

class AdminController extends Controller
{
    public $layout = '/main';//必须是/main,斜线不能去掉,否则Plugin找不到模板

    public function getAdminLayout()
    {
        $theme = Yii::$app->params['theme'];
        $this->layout = "@openadm/admin/themes/{$theme}/views/layouts/main";
    }

    public function init(){
        parent::init();

        $this->getAdminLayout();

        $this->attachBehaviors([
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new UserException('你没权限进入此页面!');
                }
            ],
        ]);
    }

}