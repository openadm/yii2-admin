<?php

use yii\bootstrap\Button;
use yii\bootstrap\Tabs;

$store_url = "http://openadm.com/addon/store";

if(isset(Yii::$app->params['store_url'])){
    $store_url = Yii::$app->params['store_url'];
}

$content = Button::widget([
    'tagName' => 'a',
    'label' => '去OpenADM云市场',
    'options' => ['href'=>$store_url,'class' => 'btn btn-info btn-lg','target'=>'_blank'],
]);

$absoluteext = Yii::getAlias('@storage/extensions');
$content .= "<div style='padding: 16px;'>";
$content .= "<div class='row'>下载后解压到 <span class='label-success'>@storage/extensions</span> 目录($absoluteext)</div>";
$content .= "<div class='row'>返回 <span class='label-primary'>本地扩展</span> 即可以看到，然后进行安装。</div>";
$content .= "</div>";
?>
<div class="nav-tabs-custom">
    <?=  Tabs::widget([
        'items' => [
            [
                'label' =>  "本地扩展",
                'url'=>['local']
            ],
            [
                'label' => '扩展商店',
                'url'=>['store'],
                'content'=> $content,
                'active' => true
            ]
        ],
    ]);
    ?>
</div>
