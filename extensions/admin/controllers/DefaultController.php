<?php
namespace openadm\admin\extensions\admin\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 * @name 后台首页
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actions()
    {
        return [
            'upload' => [
                'class' => 'kucha\ueditor\UEditorAction',
                'config' => [
                    "imageUrlPrefix"  => Yii::getAlias("@web"),
                    "imagePathFormat" => "/uploads/image/{yyyy}{mm}{dd}/{time}{rand:6}",
                    "catcherPathFormat" => "/uploads/image/{yyyy}{mm}{dd}/{time}{rand:6}",
                    "snapscreenPathFormat" => "/uploads/image/{yyyy}{mm}{dd}/{time}{rand:6}",
                    "videoPathFormat"      => "/uploads/video/{yyyy}{mm}{dd}/{time}{rand:6}",
                    "filePathFormat"       => "/uploads/file/{yyyy}{mm}{dd}/{time}{rand:6}",
                    "imageRoot" => Yii::getAlias("@webroot"),
                ],
            ]
        ];
    }
}
