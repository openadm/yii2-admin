<?php
namespace openadm\admin\web;

use Yii;
use amnah\yii2\user\models\User;
use tecnocen\oauth2server\models\OauthAccessTokens;

class Identity extends User implements \OAuth2\Storage\UserCredentialsInterface
{

    public static function tableName()
    {
        return "{{%user}}";
    }

    /**
     * Implemented for Oauth2 Interface
     */
    public static function findIdentityByAccessToken($access_token, $type = null)
    {
        $time = date('Y-m-d H:i:s');
        $module = Yii::$app->getModule('oauth2');
        $module->initOauth2Server();
        $module->getServer()->verifyResourceRequest();
        $token = $module->getServer()->getResourceController()->getToken();
        return !empty($token['user_id'])
            ? static::findIdentity($token['user_id'])
            : null;
    }

    /**
     * Implemented for Oauth2 Interface
     */
    public function checkUserCredentials($username, $password)
    {
        $user = $this->getUserByUsername($username);
        if (empty($user)) {
            return false;
        }
        return $user->validatePassword($password);
    }

    /**
     * Implemented for Oauth2 Interface
     */
    public function getUserDetails($username)
    {
        $user = $this->getUserByUsername($username);
        return ['user_id' => $user->getId()];
    }

    public function getUserByUsername($username)
    {
        $user = $this->module->model("User");
        $user = $user::find()->where(["username" => $username]);
        return $user->one();
    }

}