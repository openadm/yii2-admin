<?php
namespace openadm\admin\grid;

use yii\grid\DataColumn as YiiDataColumn;
use kartik\grid\ColumnTrait;

class DataColumn extends YiiDataColumn
{
    use ColumnTrait;
}